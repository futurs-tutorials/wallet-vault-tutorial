#!/bin/bash

function unseal_vault() {
    sleep 5
    for (( COUNTER=0; COUNTER<5; COUNTER++ ))
    do
        key=$(cat $KEYS_DIR/UNSEAL_"$COUNTER".txt)
        vault operator unseal $key
    done
}

unseal_vault