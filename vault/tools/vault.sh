#!/bin/bash

function init_vault() {
    export VAULT_INIT=$(vault operator init -format=json)
    if [[ $? -eq 2 ]] ; then
        echo "Vault initialization failed!"
        exit 2
    fi
    export VAULT_TOKEN=$(echo $VAULT_INIT | jq .root_token | tr -d '"')
    echo $VAULT_TOKEN > $KEYS_DIR/VAULT_TOKEN.txt
    for (( COUNTER=0; COUNTER<5; COUNTER++ ))
    do
        key=$(echo $VAULT_INIT | jq '.unseal_keys_hex['"$COUNTER"']' | tr -d '"')
        vault operator unseal $key
        if [[ $? -eq 2 ]]; then
            echo "FAILED TO UNSEAL VAULT AT KEY $COUNTER"
            exit 2
        fi
        echo $key > $KEYS_DIR/UNSEAL_"$COUNTER".txt
    done
    unset VAULT_INIT
}

function install_plugin() {
    cd $VAULT_DIR/plugins
    export SHASUM256=$(sha256sum vault-ethereum | awk '{ print $1 }')
    echo "SHASUM256 == $SHASUM256"
    
    # Updating the plugins catalog
    vault write sys/plugins/catalog/ethereum-plugin sha_256="${SHASUM256}" command="vault-ethereum"
    if [[ $? -eq 2 ]] ; then
        echo "Vault Catalog update failed!"
        exit 2
    fi
    
    # Mounting the Ethereum Plugin
    vault secrets enable -path=ethereum/dev -description="Futurs Ethereum Wallet" -plugin-name=ethereum-plugin plugin
    if [[ $? -eq 2 ]] ; then
        echo "Failed to mount Ethereum plugin!"
        exit 2
    fi
}

nohup vault server -config $VAULT_DIR/config/local.json &> /dev/null &
sleep 2

init_vault
if [[ $? -eq 2 ]]; then
    echo "Failed initializing Vault"
    exit 2
fi

install_plugin
if [[ $? -eq 2 ]]; then
    echo "Failed installing Vault-Ethereum"
    exit 2
fi

rm $HOME/vault-* $HOME/vault_*
kill $(ps aux | grep "vault server" | awk '{print $2}')
nohup $TOOLS_DIR/unseal_post_install.sh &> $KEYS_DIR/unseal_log.txt &
rm -rf $HOME/go $HOME/vault-* $HOME/vault_*
vault server -config $VAULT_DIR/config/local.json
