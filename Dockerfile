FROM ubuntu:latest

# Install build tools
RUN apt update --fix-missing && apt upgrade -y
RUN apt install git golang zip unzip wget curl sudo jq -y

# Add user Vault to sudo group
RUN useradd -ms /bin/bash vault
RUN echo 'vault  ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers

# Switch user to Vault
USER vault:vault
WORKDIR /home/vault
RUN mkdir go && mkdir -p etc/vault.d/config \ 
  && mkdir -p etc/vault.d/plugins \ 
  && mkdir -p etc/vault.d/logs \ 
  && mkdir -p etc/vault.d/plugins \
  && mkdir -p etc/vault.d/data \
  && mkdir -p tools

# Download and compile Ethereum plugin
RUN go get -v github.com/immutability-io/vault-ethereum
RUN go build -v github.com/immutability-io/vault-ethereum

# Make new directory for vault
RUN if [ -n "$VAULT_LOCAL_CONFIG" ]; then echo "$VAULT_LOCAL_CONFIG" > "$VAULT_CONFIG_DIR/local.json"; fi;

# Install Vault
RUN wget https://releases.hashicorp.com/vault/0.10.1/vault_0.10.1_linux_amd64.zip
RUN unzip vault_0.10.1_linux_amd64.zip && chmod +x vault && sudo cp vault /usr/local/bin

# Move plugin to plugins directory
RUN mv /home/vault/go/bin/vault-ethereum /home/vault/etc/vault.d/plugins
CMD [ "bash", "tools/vault.sh" ]
